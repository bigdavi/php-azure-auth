<?php
require_once './vendor/autoload.php';
require_once './credentials.php';
require_once './helpers.php';

handler();

function handler()
{
    // example
    // https://docs.davicloud.com/callback.php?code=M.R3_BL2.5594a9e4-662c-7dbd-01c4-dcb7caab0d9e&state=d49829879d8ee54c772ec67dae83559a
    $request = request();

    // Validar estado

    $expectedState = sessionGet('oauthState');
    sessionForget('oauthState'); // TODO: descomentar
    $providedState = $request['state']?? null;

    if (!isset($expectedState)) {
        // If there is no expected state in the session,
        // do nothing and redirect to the home page.
        die('redirect /');
        return redirect('/');
    }

    if (!isset($providedState) || $expectedState != $providedState) {
        echo "Error: Estado de autenticación invalido \n";
        echo "Detaille: El estado de autenticación proporcionado no coincide con el valor esperado";
        exit;
    }

    // Authorization code should be in the "code" query param
    $authCode = $request['code']?? null;

    if (isset($authCode)) {
        // Initialize the OAuth client
        $oauthClient = new \League\OAuth2\Client\Provider\GenericProvider([
            'clientId'                => AZURE_APP_ID,
            'clientSecret'            => AZURE_APP_SECRET,
            'redirectUri'             => AZURE_REDIRECT_URI,
            'urlAuthorize'            => AZURE_AUTHORITY . AZURE_AUTHORIZE_ENDPOINT,
            'urlAccessToken'          => AZURE_AUTHORITY . AZURE_TOKEN_ENDPOINT,
            'urlResourceOwnerDetails' => '',
            'scopes'                  => AZURE_SCOPES
        ]);

        try {
            // Make the token request
            $accessToken = $oauthClient->getAccessToken('authorization_code', [
                'code' => $authCode
            ]);

            // dd($accessToken->getToken());
            $graph = new \Microsoft\Graph\Graph();
            $graph->setAccessToken($accessToken->getToken());

            $userRequest = $graph->createRequest('GET', '/me')->execute();

            $data = $userRequest->getBody();

            $user = [
                'firstname' => $data['givenName'],
                'surname' => $data['surname'],
                'email' => $data['userPrincipalName'],
            ];

            dd($user);
            // create local session with email of azure Active Directory
            return redirect('/');
        } catch (\League\OAuth2\Client\Provider\Exception\IdentityProviderException $e) {
            $error = "Error requesting access token <br>";
            $detail = json_encode($e->getResponseBody());
            echo "$error \n $detail";
            exit;
        }
    }else{
        $error = $request['error']?? null;
        $detail = $request['error_description']?? null;
        echo "$error \n $detail";
        exit;
    }
}