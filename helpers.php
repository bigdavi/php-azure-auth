<?php

function dd($data)
{
    echo '<pre>';
        var_dump($data);
    echo '</pre>';
    die();
}

function request($key=null)
{
    if($key){
        return $_REQUEST[$key]?? null;
    }
    return $_REQUEST;
}

function redirect($url)
{
    header('Location: ' . $url);
    exit;
}

function sessionSet($key, $value)
{
    sessionStart();
    $_SESSION[$key] = $value;
    return $_SESSION[$key];
}

function sessionGet($key)
{
    sessionStart();
    return $_SESSION[$key]?? null;
}

function sessionForget($key)
{
    sessionStart();
    unset($_SESSION[$key]);
    return true;
}


function sessionStart() {
    if (session_status() == PHP_SESSION_NONE) {
        session_start();
    }
}