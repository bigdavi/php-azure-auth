<?php
require_once './vendor/autoload.php';
require_once './helpers.php';


// use Microsoft\Graph\Graph;
// use Microsoft\Graph\Model;

$token = "EwBwA8l6BAAUkj1NuJYtTVha+Mogk+HEiPbQo04AASyrZGS1RzvMUP8FpI2CJole3XBsAJz2nvMJqh0GYTqlX8PyKadhgURAUNP/iXcHIuz6W2/mO2E/HPJGOAc0FJpqoveFGgYyTIu8IsQg8Yzeu4GTptPFBGNxo1fStzoxEm1xSBKJlJODNcRLW7mn/os5+Tqv5ETv7fFhuvRH/gu4sgvG5e7P8oSVTIE2LrZY5h1xQEIhGgAH4om5mU44PRErWaoeSxIIVqiDZgkY6UOo0doc7DtIxyhbxCGHu5xY0/ZrthacfbcNhnrr96ub8AU98pUjQNrW0Nj9xNxy7EbwrwQk3DyfNla93/hupdpIWg/ebR4jdZdPbc4IrSFBEHYDZgAACODVA7eU0mH+QAL4K2DyYH0pKJuhtE5W/qCsmVC4JgvszH7pU1Mcptxmj35zLfMnM6C+DQoiv7ATsX2UgqR6CpdhlCioURqLtxCYhJY/ZsjJx6sJScrMMbkmPpU35uC8z5XAKC7OHFZgFE3IJ3iY9M6EAynNwbXdzQMjT+qTFn++J663s+p0rCalWOvooOo0culc72Hcu63zwZmsUsGsiRX0itonkdXmN3BCo6vnRx5IQi7huCGc1z41CSGqmlZsNAWdqADPHasjLD3aZsQ1TS24gi+Al2zsgp1StwBC08Xmc+GzXgd9vvpmvi8U8lPbmgQrgEfsXxZ/1IiB3jrrBb8bVUl3/iWImuAhS5NrW5Cp+waqHj+3qp6GZvgYmaUgEtBEdBxmU3wyAYaOz6Xg0Nexo2gv2VZMyD2ktBZQaR7FofBTOV5ARXnS28LmrvPwuhfVfyvDKmDveCKOz6/DvR3wcpakuERN+g12jY9F9mg5uO7HOe4B51XfUXGqYFkTK1906HYhzxbtU/uDthVZ633YKe9anqByYjl4o/wImLdPc8gJliXXEidoHZR+DefX0NQX8zj3MjN5klMMcRp2Bw1k8uUMLG1wfLMjkTXvPpzlY+nqWiZuUJXxJrutm94aPnH0pelJgCeGrPC5pXCs1tbP1V/c/hH+uLssRTq8koaNXimevV1uytKjAMoo2fENCJSLFRbwzA7MysHdHFUQt5nu8kshJQreapukwIiuaLsgK1s8ShDe/0MqjK8u9+QDZ1b1EmLawE1iD+uBAg==";

try {
    
    $graph = new \Microsoft\Graph\Graph();
    $graph->setAccessToken($token);

    $response = $graph->createRequest('GET', '/me')
        ->execute();

    $userData = $response->getBody();
    
    $user = [
        'firstname' => $userData['givenName'],
        'surname' => $userData['surname'],
        'email' => $userData['userPrincipalName'],
    ];

    dd($user);
} catch (\League\OAuth2\Client\Provider\Exception\IdentityProviderException $e) {

    $error = "Error requesting access token"; 
    $detail = "Detail: "+ json_encode($e->getResponseBody()); 

    echo "$error \n$detail";
}
