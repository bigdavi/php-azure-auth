<?php

require_once './vendor/autoload.php';
require_once './credentials.php';
require_once './helpers.php';

signin();

function signin()
{

    // Initialize the OAuth client
    $oauthClient = new \League\OAuth2\Client\Provider\GenericProvider([
        'clientId'                => AZURE_APP_ID,
        'clientSecret'            => AZURE_APP_SECRET,
        'redirectUri'             => AZURE_REDIRECT_URI,
        'urlAuthorize'            => AZURE_AUTHORITY . AZURE_AUTHORIZE_ENDPOINT,
        'urlAccessToken'          => AZURE_AUTHORITY . AZURE_TOKEN_ENDPOINT,
        'urlResourceOwnerDetails' => '',
        'scopes'                  => AZURE_SCOPES
    ]);

    $authUrl = $oauthClient->getAuthorizationUrl();

    // Save client state so we can validate in callback
    sessionSet('oauthState', $oauthClient->getState());

    // Redirect to AAD signin page
    return redirect($authUrl);
}