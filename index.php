<?php
    require_once './helpers.php';

    $redirectUrl = $_SERVER['REDIRECT_URL']?? null;

    if( $redirectUrl === '/signin-oidc'){
        $queryParams = explode('?', $_SERVER['REQUEST_URI'])[1];
        redirect('/signin-oidc.php?'.$queryParams);
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Docs - Davicloud</title>
</head>
<body>
    <a href="./auth.php" target="_blank">Ingresar con Azure</a>
    <!-- <a href="./go-auth.php" target="_blank">Ingresar con Azure</a> -->
</body>
</html>