# Requisitos
- [ ] apache
- [ ] php 7.3 o suporior
- [ ] composer

# Clonar el repositorio
ingrese en sus directorio de sitios de apache y ejecute el siguiente comando:
git clone git@gitlab.com:bigdavi/php-azure-auth.git ./docs.davicloud.com

# Instale dependecias composer
composer install

# Configure un host virtual apache
Configure un host virtual para "https//docs.davicloud.com" y apunte al directorio clonado "<apache_www>/docs.davicloud.com"