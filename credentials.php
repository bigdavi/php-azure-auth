<?php
// Active Directory: UNACEM
const AZURE_TENANT_ID = "6d252419-40fe-4964-bb31-e8aaa96d9766";
const AZURE_APP_ID = "6d9753a9-ce00-4a0d-a60a-776dd9bc2cb6";
const AZURE_APP_SECRET = "";
const AZURE_REDIRECT_URI = "https://docs.davicloud.com/signin-oidc";
const AZURE_SCOPES = 'openid user.read';
// const AZURE_AUTHORITY = "https://login.microsoftonline.com/common/";
const AZURE_AUTHORITY = "https://login.microsoftonline.com/".AZURE_TENANT_ID.'/';
const AZURE_AUTHORIZE_ENDPOINT = "/oauth2/v2.0/authorize";
const AZURE_TOKEN_ENDPOINT = "/oauth2/v2.0/token";